import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userData: any=[];
  login: any ={};
  constructor(private route:Router) { 
    console.log(JSON.parse(localStorage.getItem('users')));
    this.userData = JSON.parse(localStorage.getItem('users'));
  }

  ngOnInit() {
  }

  submit(){
    for (let i = 0; i < this.userData.length; i++) {
      console.log(this.userData);
      if(this.userData[i].username==this.login.userId && this.userData[i].password==this.login.password){
        this.route.navigate(['/userdetails',this.userData[i]]);
       break;
      }else{
        alert('username/password invalid');
       // break;
      }
      
    }
   
    console.log(this.userData)
  }

}
