import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from "@angular/forms";
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { RouterModule,Routes } from "@angular/router";
import { UserDetailsComponent } from './user-details/user-details.component';


const approot: Routes = [
  {path:"signup",component:SignupComponent},
  {path:"login",component:LoginComponent},
  {path:"userdetails",component:UserDetailsComponent},
  { path: "", redirectTo: "/login", pathMatch: "full" }

];
@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    UserDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(approot),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
