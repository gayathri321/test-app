import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  userdetails: any;
  userData: any;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.userdetails = this.route.params.subscribe(params => {
      this.userData = params;
      console.log(this.userData);
    })
  }

}
